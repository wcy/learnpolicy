#pragma once
#include "cymat.h"

cyMat<int> randperm(int K)
{
	vector<int> v(K);
	for (int i = 0; i < K; i++)
		v[i] = i;
	random_shuffle(v.begin(), v.end());
	cyMat<int> rt(K, 1);
	for (int i = 0; i < K; i++)
		rt.val(i) = v[i];
	return rt;
}