# pragma once

#include "LearnPolicy.h"
#include <limits.h>
#include <vector>

#define EPS 0.000000001
#ifdef DEBUG
#include "cymatmex.h"
#endif

// minimizing cost given fixed FN
/*
cyMat<double> greedyRefineRejLine(const cyMat<double> &Spos, const cyMat<int> &filled_idxs, const cyMat<double> &filled_values)
{
	int N = Spos._h;
	int T = Spos._w;
	cyMat<bool> is_detected(N, 1, true);
	for (int x = 0; x < N; x++)
	{
		for (int t = 0; t < filled_idxs.size(); t++)
		{
			if (Spos.val(x, filled_idxs.val(t)) < filled_values.val(t))
			{
				is_detected.val(x) = false;
				break;
			}
		}
	}

	cyMat<double> rejline(T, 1, std::numeric_limits<double>::max());
	for (int x = 0; x < N; x++)
	{
		for (int t = 0; t < T; t++)
		{
			if (is_detected.val(x))
				rejline.val(t) = std::min(rejline.val(t), Spos.val(x, t));
		}
	}
	return rejline;
}
*/
/*
cyMat<double> greedyRefinePolicy(const cyMat<double> &Spos, const cyMat<double> &Sneg, const cyMat<double> &policy)
{
	int T = Spos._w;
	cyMat<double> rt(policy._h, 2);
	for (int t = 0; t < T; t++)
	{
		rt.val(t, 0) = policy.val(t, 1);
	}
	
	for (int x = 0; x < Spos._h; x++)
	{
		bool is_detected = true;
		int t;
		for (t = 0; t < T; t++)
		{
			if (Spos.val(x, t) < policy.val(t, 0))
			{
				is_detected = false;
				break;
			}
			if (Spos.val(x, t) >= policy.val(t, 1))
				break;
		}
		if (is_detected)
			for (int i = 0; i <= min(t, T-2); i++)
				rt.val(i, 0) = min(rt.val(i, 0), Spos.val(x, i));
	}
	
	for (int t = 0; t < T; t++)
	{
		rt.val(t, 1) = rt.val(t, 0);
	}

	for (int x = 0; x < Sneg._h; x++)
	{
		bool is_rejected = true;
		int t;
		for (t = 0; t < T; t++)
		{
			if (Sneg.val(x, t) >= policy.val(t, 1))
			{
				is_rejected = false;
				break;
			}
			if (Sneg.val(x, t) < rt.val(t, 0))
				break;
		}
		if (is_rejected)
			for (int i = 0; i <= min(t, T-2); i++)
				rt.val(i, 1) = max(rt.val(i, 1), Sneg.val(x, i)+EPS);
	}
	
	return rt;
}
*/

cyMat<double> greedyRefinePolicy(const cyMat<double> &Spos, const cyMat<double> &Sneg, const cyMat<double> &policy)
{
	int T = Spos._w;
	cyMat<double> rt(T, 2);
	cyMat<double> acp_line(T, 1);
	cyMat<double> rej_line(T, 1);
	for (int t = 0; t < T; t++)
	{
		rej_line.val(t) = policy.val(t, 0);
		acp_line.val(t) = policy.val(t, 1);
	}
	refineRejline(rej_line, acp_line, Spos);
	refineAcpline(rej_line, acp_line, Sneg);
	for (int t = 0; t < T; t++)
	{
		rt.val(t, 0) = rej_line.val(t);
		rt.val(t, 1) = acp_line.val(t);
	}
	return rt;
}
/*
// remove pos/negative samples which never cross the 'gray zone'
void filterTrainData(cyMat<double> &Spos_out, cyMat<double> &Sneg_out, const cyMat<double> &Spos, const cyMat<double> &Sneg)
{
	int Npos = Spos._h;
	int Nneg = Sneg._h;
	int T = Spos._w;
	cyMat<double> pos_min = min(Spos);
	cyMat<double> neg_max = max(Sneg);
#ifdef DEBUG
	mexPrintf("Spos min:\n");
	mexPrintCyMat(pos_min);
	mexPrintf("Sneg max:\n");
	mexPrintCyMat(neg_max);
#endif
	int Npos_out = 0;
	int Nneg_out = 0;
	int* idx_pos_out = new int[Npos];
	int* idx_neg_out = new int[Nneg];
	// record filtered positive sample ids
	for (int x = 0; x < Npos; x++)
	{
		bool flag = false;
		for (int t = 0; t < T; t++)
			if (Spos.val(x, t) < neg_max.val(t))
			{
			flag = true;
			break;
			}
		if (flag)
			idx_pos_out[Npos_out++] = x;
	}
	// record filtered negative sample ids
	for (int x = 0; x < Nneg; x++)
	{
		bool flag = false;
		for (int t = 0; t < T; t++)
			if (Sneg.val(x, t) > pos_min.val(t))
			{
			flag = true;
			break;
			}
		if (flag)
			idx_neg_out[Nneg_out++] = x;
	}
	// write output matrix
	Spos_out.init(Npos_out, T);
	Sneg_out.init(Nneg_out, T);
	for (int i = 0; i < Npos_out; i++)
		for (int t = 0; t < T; t++)
			Spos_out.val(i, t) = Spos.val(idx_pos_out[i], t);
	for (int i = 0; i < Nneg_out; i++)
		for (int t = 0; t < T; t++)
			Sneg_out.val(i, t) = Sneg.val(idx_neg_out[i], t);
	delete[] idx_pos_out;
	delete[] idx_neg_out;

}
*/

// one step coordinate descent to optimize rejection line
// modify the given initual rejection line: 'rejline' at 'idx' dim
// output true if rejection line is modified, otherwise, false
bool cdRejLine(cyMat<int> &rejline, const cyMat<int> &acpline, const cyMat<int> &Spos, const cyMat<int> &Sneg, const cyMat<double> &C, double lambda, int binnum, int idx)
{
	int Npos = Spos._h;
	int Nneg = Sneg._h;
	int T = Spos._w;
	cyMat<double> T1(binnum, 1, 0), T3(binnum, 1, 0);
	cyMat<int> T2(binnum, 1, 0), T4(binnum, 1, 0);
	double t1 = 0;
	int t2 = 0;
	//FN = T2+t2-T4g
	//cost = T1+t1-T3

	// compute T1, t1, T3
	// cost for negative samples
	for (int x = 0; x < Nneg; x++)
	{
		bool is_decided = false;
		// before idx
		for (int t = 0; t < idx; t++)
		{
			if (Sneg.val(x, t) < rejline.val(t) || Sneg.val(x, t) >= acpline.val(t))
			{
				t1 += C.val(t);
				is_decided = true;
				break;
			}
		}
		if (is_decided)
			continue;
		// at idx
		if (Sneg.val(x, idx) >= acpline.val(idx))
		{
			t1 += C.val(idx);
			continue;
		}
		T1.val(Sneg.val(x, idx)) += C.val(idx);
		// after idx
		for (int t = idx + 1; t < T; t++)
		{
			if (Sneg.val(x, t) < rejline.val(t) || Sneg.val(x, t) >= acpline.val(t))
			{
				t1 += C.val(t);
				T3.val(Sneg.val(x, idx)) += C.val(t);
				break;
			}
		}
	}

	for (int x = 0; x < Npos; x++)
	{
		bool is_decided = false;
		// before idx
		for (int t = 0; t < idx; t++)
		{
			if (Spos.val(x, t) < rejline.val(t))
			{
				t2++;
				is_decided = true;
				break;
			}

			if (Spos.val(x, t) >= acpline.val(t))
			{
				is_decided = true;
				break;
			}
		}
		if (is_decided)
			continue;
		// at idx
		if (Spos.val(x, idx) >= acpline.val(idx))
			continue;
		T2.val(Spos.val(x, idx))++;
		// after idx
		for (int t = idx + 1; t < T; t++)
		{
			if (Spos.val(x, t) < rejline.val(t))
			{
				t2++;
				T4.val(Spos.val(x, idx))++;
				break;
			}

			if (Spos.val(x, t) >= acpline.val(t))
				break;
		}
	}

	// compute T1 T2 T3 T4 by cumsum
	for (int b = 1; b < acpline.val(idx); b++)
	{
		T1.val(b) += T1.val(b - 1);
		T2.val(b) += T2.val(b - 1);
		T3.val(b) += T3.val(b - 1);
		T4.val(b) += T4.val(b - 1);
	}
	// cumpute Tables of cost and fn via T1, T2, T3, T4, t1, t2
	cyMat<int> Tfn(binnum, 1);
	cyMat<double> Tcost(binnum, 1);
	for (int b = 1; b <= acpline.val(idx); b++)
	{
		Tcost.val(b) = t1 + T1.val(b - 1) - T3.val(b - 1);
		Tfn.val(b) = t2 + T2.val(b - 1) - T4.val(b - 1);
	}
	Tcost.val(0) = t1;
	Tfn.val(0) = t2;

	int next_b = rejline.val(idx);
	double min_risk = std::numeric_limits<double>::max();
	for (int b = acpline.val(idx); b >= 0; b--)
	{
		double risk = Tcost.val(b) / (double)Nneg + lambda*(Tfn.val(b)) / (double)Npos;
		if (risk < min_risk)
		{
			min_risk = risk;
			next_b = b;
		}
	}
	if (next_b != rejline.val(idx))
	{
		rejline.val(idx) = next_b;
		
#ifdef DEBUG
		mexPrintf("%.4f c:%f fn: %d\n", min_risk, Tcost.val(next_b), Tfn.val(next_b));
#endif
		return true;
	}
	return false;
}


/*
void refineRejline(cyMat<int> &rejline, const cyMat<int> &acpline, const cyMat<int> &Spos)
{
	int N = Spos._h; // number of samples
	int T = Spos._w; // number of stages

	cyMat<int> init_rejline = rejline;
	// initialize the rej thresholds to the maximum valid value
	for (int t = 0; t < T - 2; t++)
		rejline.val(t) = acpline.val(t);

	for (int x = 0; x < N; x++)
	{
		bool is_detected = true;
		int t = 0;
		for (t = 0; t < T; t++)
		{
			if (Spos.val(x, t) < init_rejline.val(t)) // early rejected
			{
				is_detected = false;
				break;
			}
			if (Spos.val(x, t) >= acpline.val(t)) // early accepted
				break;
		}
		if (is_detected)
		{
			for (int i = 0; i <= min(t, T-2); i++) // for stages where the sample passes
				rejline.val(i) = min(rejline.val(i), Spos.val(x, i));
		}
	}
}
*/

cyMat<int> optmizeRejLine(const cyMat<int> &init_rejline, const cyMat<int> &acpline, const cyMat<int> &Spos, const cyMat<int> &Sneg, const cyMat<double> &C, double lambda, int binnum)
{
	cyMat<int> rejline = init_rejline;
	
	bool is_modified = true;
	int T = Spos._w;
	refineRejline(rejline, acpline, Spos);

	try
	{
		while (is_modified)
		{
			is_modified = false;
			cyMat<int>idxlist = randperm(T - 1);
			//mexPrintCyMat(idxlist);
			for (int i = 0; i < T - 1; i++)
			{
				bool flag = cdRejLine(rejline, acpline, Spos, Sneg, C, lambda, binnum, idxlist.val(i));
				if (flag)
				{
					refineRejline(rejline, acpline, Spos);
				}
				is_modified = is_modified || flag;
			}
		}
	}
	catch (exception& e)
	{
#ifdef DEBUG
		mexPrintf("exception\n");
#endif
	}
	
	
	return rejline;
}

/*
void refineAcpline(const cyMat<int> &rejline,  cyMat<int> &acpline, const cyMat<int> &Sneg)
{
	int N = Sneg._h;
	int T = Sneg._w;
	cyMat<int> init_acpline = acpline;
	for (int t = 0; t < T - 2; t++)
		acpline.val(t) = rejline.val(t);
	for (int x = 0; x < N; x++)
	{
		bool is_rejected = true;
		int t = 0;
		for (t = 0; t < T; t++)
		{
			if (Sneg.val(x, t) < rejline.val(t))
				break;
			if (Sneg.val(x, t) >= init_acpline.val(t))
			{
				is_rejected = false;
				break;
			}

		}

		if (is_rejected)
		{
			for (int i = 0; i <= min(t, T - 2); i++)
				acpline.val(i) = max(acpline.val(i), Sneg.val(x, i)+1);
		}

	}
}
*/
// one step coordinate descent to optimize acceptance line
// modify the given initial accept line: 'acpline' at 'idx' dim
// output true if accept line is modified, otherwise, false
bool cdAcpLine(const cyMat<int> &rejline,  cyMat<int> &acpline, const cyMat<int> &Spos, const cyMat<int> &Sneg, const cyMat<double> &C, double lambda, int binnum, int idx)
{
	int Npos = Spos._h;
	int Nneg = Sneg._h;
	int T = Spos._w;
	cyMat<double> T1(binnum, 1, 0), T3(binnum, 1, 0);
	cyMat<int> T2(binnum, 1, 0), T4(binnum, 1, 0);
	double t1 = 0;
	int t2 = 0;
	//FN = T2+t2-T4g
	//cost = T1+t1-T3

	// compute T1, t1, T3
	// cost for positive samples
	for (int x = 0; x < Npos; x++)
	{
		bool is_decided = false;
		// before idx
		for (int t = 0; t < idx; t++)
		{
			if (Spos.val(x, t) < rejline.val(t) || Spos.val(x, t) >= acpline.val(t))
			{
				t1 += C.val(t);
				is_decided = true;
				break;
			}
		}
		if (is_decided)
			continue;
		// at idx
		if (Spos.val(x, idx) < rejline.val(idx))
		{
			t1 += C.val(idx);
			continue;
		}
		T1.val(Spos.val(x, idx)) += C.val(idx);
		// after idx
		for (int t = idx + 1; t < T; t++)
		{
			if (Spos.val(x, t) < rejline.val(t) || Spos.val(x, t) >= acpline.val(t))
			{
				t1 += C.val(t);
				T3.val(Spos.val(x, idx)) += C.val(t);
				break;
			}
		}
	}

	for (int x = 0; x < Nneg; x++)
	{
		bool is_decided = false;
		// before idx
		for (int t = 0; t < idx; t++)
		{
			if (Sneg.val(x, t) < rejline.val(t))
			{
				is_decided = true;
				break;
			}

			if (Sneg.val(x, t) >= acpline.val(t))
			{
				t2++;
				is_decided = true;
				break;
			}
		}
		if (is_decided)
			continue;
		// at idx
		if (Sneg.val(x, idx) < rejline.val(idx))
			continue;
		T2.val(Sneg.val(x, idx))++;
		// after idx
		for (int t = idx + 1; t < T; t++)
		{
			if (Sneg.val(x, t) < rejline.val(t))
			{
				break;
			}

			if (Sneg.val(x, t) >= acpline.val(t))
			{
				t2++;
				T4.val(Sneg.val(x, idx))++;
				break;
			}
				
		}
	}

	// compute T1 T2 T3 T4 by cumsum
	for (int b = binnum-2; b >=rejline.val(idx); b--)
	{
		T1.val(b) += T1.val(b + 1);
		T2.val(b) += T2.val(b + 1);
		T3.val(b) += T3.val(b + 1);
		T4.val(b) += T4.val(b + 1);
	}
	// cumpute Tables of cost and fn via T1, T2, T3, T4, t1, t2
	cyMat<int> Tfp(binnum, 1);
	cyMat<double> Tcost(binnum, 1);
	for (int b = binnum -2; b >= rejline.val(idx); b--)
	{
		Tcost.val(b) = t1 + T1.val(b) - T3.val(b);
		Tfp.val(b) = t2 + T2.val(b) - T4.val(b);
	}
	Tcost.val(binnum-1) = t1;
	Tfp.val(binnum-1) = t2;

	//mexPrintCyMat(Tcost);
	//mexPrintf("Tfp\n");
	//mexPrintCyMat(Tfp);

	int next_b = acpline.val(idx);
	double min_risk = std::numeric_limits<double>::max();
	for (int b = rejline.val(idx); b < binnum; b++)
	{
		double risk = Tcost.val(b) / (double)Npos + lambda*(Tfp.val(b)) / (double)Nneg;
		if (risk < min_risk)
		{
			min_risk = risk;
			next_b = b;
		}
	}
	if (next_b != acpline.val(idx))
	{
		acpline.val(idx) = next_b;

#ifdef DEBUG
		mexPrintf("%.4f c:%f fp: %d\n", min_risk, Tcost.val(next_b), Tfp.val(next_b));
#endif
		return true;
	}
	return false;
}

cyMat<int> optmizeAcpLine(const cyMat<int> &rejline, const cyMat<int> &init_acpline, const cyMat<int> &Spos, const cyMat<int> &Sneg, const cyMat<double> &C, double lambda, int binnum)
{
	cyMat<int> acpline = init_acpline;

	bool is_modified = true;
	int T = Spos._w;
	refineAcpline(rejline, acpline, Sneg);

	try{
		while (is_modified)
		{
			is_modified = false;
			cyMat<int>idxlist = randperm(T - 1);
			//mexPrintCyMat(idxlist);
			for (int i = 0; i < T - 1; i++)
			{
				bool flag = cdAcpLine(rejline, acpline, Spos, Sneg, C, lambda, binnum, idxlist.val(i));
				if (flag)
				{
					refineAcpline(rejline, acpline, Sneg);
				}
				is_modified = is_modified || flag;
			}
			//break;
		}
	}
	catch (exception& e)
	{
#ifdef DEBUG
		mexPrintf("exception\n");
#endif
	}


	return acpline;
}

/*
cyMat<int> greedyRefinePolicy_binids(const cyMat<int> &Spos, const cyMat<int> &Sneg, const cyMat<int> &policy)
{
	int T = Spos._w;
	cyMat<int> rt(policy._h, 2);
	for (int t = 0; t < T; t++)
	{
		rt.val(t, 0) = policy.val(t, 1);
	}
	
	for (int x = 0; x < Spos._h; x++)
	{
		bool is_detected = true;
		int t;
		for (t = 0; t < T; t++)
		{
			if (Spos.val(x, t) < policy.val(t, 0))
			{
				is_detected = false;
				break;
			}
			if (Spos.val(x, t) >= policy.val(t, 1))
				break;
		}
		if (is_detected)
			for (int i = 0; i <= min(t, T-2); i++)
				rt.val(i, 0) = min(rt.val(i, 0), Spos.val(x, i));
	}

	for (int t = 0; t < T; t++)
	{
		rt.val(t, 1) = rt.val(t, 0);
	}

	
	for (int x = 0; x < Sneg._h; x++)
	{
		bool is_rejected = true;
		int t;
		for (t = 0; t < T; t++)
		{
			if (Sneg.val(x, t) >= policy.val(t, 1))
			{
				is_rejected = false;
				break;
			}
			if (Sneg.val(x, t) < rt.val(t, 0))
				break;
		}
		if (is_rejected)
			for (int i = 0; i <= min(t, T - 2); i++)
				rt.val(i, 1) = max(rt.val(i, 1), Sneg.val(x, i)+1);
	}

	return rt;
}
*/

