#pragma once


//#include "cymatmex.h"


#include "cymat.h"

cyMat<double> greedyRefinePolicy(const cyMat<double> &Spos, const cyMat<double> &Sneg, const cyMat<double> &policy);

// cd optimization with random order iteration. In each iteration, a random threshold is adjusted by cd, then the rest thresholds are adjusted by the 'refineRejline' func.
cyMat<int> optmizeRejLine(const cyMat<int> &init_rejline, const cyMat<int> &acpline, const cyMat<int> &Spos, const cyMat<int> &Sneg, const cyMat<double> &C, double lambda, int binnum);

cyMat<int> optmizeAcpLine(const cyMat<int> &rejline, const cyMat<int> &init_acpline, const cyMat<int> &Spos, const cyMat<int> &Sneg, const cyMat<double> &C, double lambda, int binnum);

// maximize rejection thresholds such that no positive sample is rejected if it's originally accepted by the initial policy
// the thresholds are set as the minimum of score value of such positive samples.
template<typename F>
void refineRejline(cyMat<F> &rejline, const cyMat<F> &acpline, const cyMat<F> &Spos)
{
	int N = Spos._h; // number of samples
	int T = Spos._w; // number of stages

	cyMat<F> init_rejline = rejline;
	// initialize the rej thresholds to the maximum valid value
	for (int t = 0; t < T - 2; t++)
		rejline.val(t) = acpline.val(t);

	for (int x = 0; x < N; x++)
	{
		bool is_detected = true;
		int t = 0;
		for (t = 0; t < T; t++)
		{
			if (Spos.val(x, t) < init_rejline.val(t)) // early rejected
			{
				is_detected = false;
				break;
			}
			if (Spos.val(x, t) >= acpline.val(t)) // early accepted
				break;
		}
		if (is_detected)
		{
			for (int i = 0; i <= min(t, T - 2); i++) // for stages where the sample passes
				rejline.val(i) = min(rejline.val(i), Spos.val(x, i));
		}
	}
}

// minimize accept thresholds such that no negative sample is accepted if it's originally rejected by the initial policy
template<typename F>
void refineAcpline(const cyMat<F> &rejline, cyMat<F> &acpline, const cyMat<F> &Sneg)
{
	int N = Sneg._h;
	int T = Sneg._w;
	cyMat<F> init_acpline = acpline;
	F eps;
	if (typeid(F) == typeid(int))
		eps = 1;
	else
		eps = 0.000000001;

	for (int t = 0; t < T - 2; t++)
		acpline.val(t) = rejline.val(t);
	for (int x = 0; x < N; x++)
	{
		bool is_rejected = true;
		int t = 0;
		for (t = 0; t < T; t++)
		{
			if (Sneg.val(x, t) < rejline.val(t))
				break;
			if (Sneg.val(x, t) >= init_acpline.val(t))
			{
				is_rejected = false;
				break;
			}

		}
		if (is_rejected)
		{
			for (int i = 0; i <= min(t, T - 2); i++)
				acpline.val(i) = max(acpline.val(i), Sneg.val(x, i) + eps);
		}

	}
}