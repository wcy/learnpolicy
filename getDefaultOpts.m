function opts = getDefaultOpts(weak_learner_num, tgfn, tgfp)
% set default options for optimizePolicy
%
% USAGE
%   opts = getDefaultOpts(weak_learner_num, tgfn) [only optimize rejection thresholds]
%   opts = getDefaultOpts(weak_learner_num, tgfn, tgfp) [optimize both reject&accept thresholds]
%   
% INPUT
%   weak_learner_num - weak learner number of the additive classifier
%   tgfn - target false negative rate
%   tgfp - target false positive rate
%
% OUTPUT
%   opts - see optimizePolicy.m

assert(nargin>=2)
opts.tgfn = tgfn;
opts.fn_epsilon = max(tgfn/100, eps);
if nargin == 2 % only optimize reject threshold
    opts.mode = 'rej';
elseif nargin == 3 % also optimize accept threshold
    opts.mode = 'full';
    opts.tgfp = tgfp;
    opts.fp_epsilon = max(tgfp/100, eps);
end
opts.bin_num = 10000;
opts.lambda_epsilon = 0.001;
STAGE_NUM = 20;
if weak_learner_num<=STAGE_NUM
    opts.stage_end_idx = 1:weak_learner_num;
else
    % divide scores(weak learners) evenly into 20 stages
    opts.stage_end_idx = round(flip(weak_learner_num:-weak_learner_num/STAGE_NUM:weak_learner_num/STAGE_NUM));    
end