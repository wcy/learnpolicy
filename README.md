# Learning Policy #

This is a simple Matlab tool to optimize decision thresholds(early reject/accept thresholds) for cascaded classifiers. The objective is to minimize the classifier's computational cost given a user-specified target accuracy(targert FPrate/FNrate):

* min Cost, s.t. FP <= target FP, FN <= target FN

Instead of jointly optimize both early reject and accept thresholds as proposed in the reference paper, this implementation iteratively optimize these two kinds of thresholds separately in a coordinate descent manner:

* given a classification threshold for the last stage of the classifier, fixed by user
* iteratively optimize the following 2 objectives till converge:
* optimize early reject thresholds: min Cost negative samples, s.t. FN <= target FN
* optimize early accept thresholds: min Cost positive samples, s.t. FP <= target FP

This approach is gauraunteed to converge, cost less memory and run faster.

### Install ###

* cd private
* Compile

### Usage ###
* see demo.m
### Reference ###

* Tianfu Wu and Song-Chun Zhu, Learning Near-Optimal Cost-Sensitive Decision Policy for Object Detection, IEEE Trans. on Pattern Analysis and Machine Intelligence (TPAMI), 37(5): 1013-1027, 2014.