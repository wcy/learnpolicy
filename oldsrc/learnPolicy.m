function [policy, Info] = learnPolicy(scores, labels, evalcost, classify_line, epsilon, opts)
% Optimize early rejection lines for additive binary classifiers.
% INPUT
%   scores(N*T matrix): decision values for each training samples at each stage.
%   labels(N-dim vector): classification label for each sample.
%   evalcost(T-dim vector): evaluation cost for each stage.
%   classify_line(scalar): classification line for the last stage.
%   epsilon(scalar): target FN rate increase.
%   opts(structure): options
%       'bin_num'[default = 100]: number of bins for decision value
%           discretization.
%       'delta'[default = epsilon/5]: algorithm stops if abs('current
%           FPrate increase' - epsilon) < delta.
% OUTPUT
%   policy(T*2 matrix): reject and accept lines for each stage. The last
%       row = [classify_line, classify_line].
%   Info(structure): mid-results.
%

if isfield(opts, 'bin_num'), bin_num = opts.bin_num; else bin_num = 100; end
if isfield(opts, 'delta'), delta = opts.delta; else delta = epsilon/5; end
if ~isfield(opts, 'refine'),  opts.refine = true; end
if ~isfield(opts, 'isbrutal'), opts.isbrutal = false; end

Info = [];
labels(labels<=0) = -1;
labels(labels>0) = 1;
% labels(labels>0 & scores(:,end) < classify_line) = -1;
% compute FP upper bound and FN lower bound
% FP_upper_bnd = sum(scores(:, end)>=classify_line & labels<0)/sum(labels<0);
FN_lower_bnd = sum(scores(:, end)<classify_line & labels>0)/sum(labels>0);

bin_edges = constructBin(scores, bin_num);
[T1,  T3, T4, T6, binids] = computeDPtables(bin_edges, scores, labels, evalcost, classify_line);
save('tables.mat', 'T1',  'T3', 'T4',  'T6')


tg_FN = FN_lower_bnd + epsilon; % target FP rate

if ~opts.isbrutal
    % binary search for CFN
    S = length(labels);
    CFNmax = length(labels) / (max(epsilon, 0.1/S) * sum(labels>0));
    CFNmin = 0;
    if isfield(opts, 'CFN')
        CFNmax = opts.CFN;
        CFNmin = opts.CFN;
    end
    itr_count = 1;
    policies = {};
    alphas = [];
    FNrates = [];
    FPrates = [];
    avgCosts = [];
    FN_dp = [];
    FN_upper = [];
    cost_dp = [];
    cost_upper = [];
    upperbounds = [];
    while true
        CFN = (CFNmax + CFNmin)/2;
        [policy, upperbound] = DPsolver(CFN, T1, T3, T4, T6);
        [risk0, FPrate0, FNrate0, avgCost0] = evalRisk(CFN, evalcost, policy, binids, labels);
        [avgCost_up, FNrate_up] = evalUpperbound(T1, T3, T4, T6, policy, labels);
        if opts.refine
            policy = refineDP(policy, CFN, evalcost, binids, labels, bin_num);
        end
        [risk, FPrate, FNrate, avgCost] = evalRisk(CFN, evalcost, policy, binids, labels);
        
%         avgCost_up + FNrate_up*sum(labels>0)/length(labels)*CFN
        policies{itr_count} = constructRealPolicy(policy, bin_edges, classify_line);
        [alphas(itr_count), FNrates(itr_count), FPrates(itr_count), avgCosts(itr_count), ...
            FN_dp(itr_count), cost_dp(itr_count), ...
            FN_upper(itr_count), cost_upper(itr_count), upperbounds(itr_count)] = ...
            deal(CFN, FNrate, FPrate, avgCost, FNrate0, avgCost0, FNrate_up, avgCost_up*sum(evalcost), upperbound);
        fprintf('itr: %d (CFN: %f),  upperbound %f, actual risk: %f ( %f),  FP: %f (%f), FN: %f (%f), C: %f(%f) \n', ...
            itr_count, CFN, upperbound, risk, risk0, FPrate, FPrate0, FNrate, FNrate0, avgCost, avgCost0);
        %         fprintf('itr: %d (CFN: %f), FN: %f(%f), FP: %f, C: %f(%f), risk %f (%f)\n', ...
        %             itr_count, CFN, FNrate, FNrate_up, FPrate, avgCost, sum(evalcost)*avgCost_up, risk, upperbound);
        if  abs(FNrate - tg_FN)<delta
            fprintf('success.: delta = %f\n', abs(FNrate - tg_FN));
            break;
        end
        if CFNmax - CFNmin < 0.001
            fprintf('fail.: delta = %f\n', abs(FNrate - tg_FN));
            break;
        end
        if FNrate >= tg_FN
            CFNmin = CFN;
        else
            CFNmax = CFN;
        end
        itr_count = itr_count+1;
    end
    policy = constructRealPolicy(policy, bin_edges, classify_line);
    
else
    % brutal search for CFN
    S = length(labels);
    CFNmax = length(labels) / (max(epsilon, 0.1/S) * sum(labels>0));
    alphas = CFNmax/50:CFNmax/50:CFNmax;
%     alphas = 3;
    policies = cell(length(alphas), 1);
    FPrates = zeros(length(alphas), 1);
    FNrates = zeros(length(alphas), 1);
    avgCosts = zeros(length(alphas), 1);
    cost_upper = zeros(length(alphas), 1);
    FN_upper = zeros(length(alphas), 1);
    FN_dp = zeros(length(alphas), 1);
    cost_dp = zeros(length(alphas), 1);
    upperbounds = zeros(length(alphas), 1);
    for i=1:length(alphas)
        CFN = alphas(i);
        [policy, upperbounds(i)] = DPsolver(CFN, T1, T3, T4, T6);
        [risk, ~, FN_dp(i), cost_dp(i)] = ...
            evalRisk(CFN, evalcost, policy, binids, labels);
        [cost_upper(i), FN_upper(i)] = evalUpperbound(T1, T3, T4, T6, policy, labels);
        
        policy = refineDP(policy, CFN, evalcost, binids, labels, bin_num);
        [~, FPrates(i), FNrates(i), avgCosts(i)] = ...
            evalRisk(CFN, evalcost, policy, binids, labels);
        policies{i} = constructRealPolicy(policy, bin_edges, classify_line);
    end
    [val, idx] = min(abs(FNrates - tg_FN));
    policy = policies{idx};
    
end
Info.FPrates = FPrates;
Info.FNrates = FNrates;
Info.avgCosts = avgCosts;
Info.FN_upper = FN_upper;
Info.cost_upper = cost_upper;
Info.FN_dp = FN_dp;
Info.cost_dp = cost_dp;
Info.policies = policies;
Info.alphas = alphas;
Info.upperbounds = upperbounds;

function bin_edges = constructBin(dsvals, bin_num)
bin_edges = zeros(bin_num+1, size(dsvals, 2));
bin_edges(1,:) = -inf;
bin_edges(end,:) = inf;
for i=1:size(dsvals, 2)
    step_length = (max(dsvals(:,i)) - min(dsvals(:,i)))/(bin_num-2);
    bin_edges(2:end-1,i) = min(dsvals(:,i)):step_length:min(dsvals(:,i))+step_length*(bin_num-2);
end

function [T1,  T3, T4, T6, binids] = computeDPtables(bin_edges, scores, labels, evalcost, classify_line)
stage_num = size(scores, 2);
bin_num = size(bin_edges, 1)-1;

% prepare data for DP tables
S_N = zeros(bin_num, stage_num-1); % store sample # under each lower bound
S_FN = zeros(bin_num, stage_num-1);  % S+(\tau-)

binids = zeros(length(labels), stage_num);
binids(scores(:,end)<classify_line, end) = 1;
binids(scores(:,end)>=classify_line, end) = 2;

for i=1:stage_num-1
    [bincounts, binids(:,i)] = histc(scores(:,i), bin_edges(:,i));
    fn_bincounts = histc(scores(labels>0, i), bin_edges(:,i));
    S_FN(:,i) = fn_bincounts(1:end-1); % count false negative sample # in each bin, if the next bin is lower bound
    S_N(:,i) = bincounts(1:end-1); % count sample # in each bin
end

S_FN = cumsum(S_FN, 1);
S_FN = [zeros(1, stage_num-1); S_FN(1:end-1, :)]; % S+(\tau-) false negative # if the bin is the lowser bound
S_N = cumsum(S_N, 1);
S_N = [zeros(1, stage_num-1); S_N(1:end-1, :)]; % S(\tau-) sample # under the lower bound

S2 = zeros(bin_num, bin_num, stage_num-1); % count sample # in each bin (c, p)
S2_FN = zeros(bin_num, bin_num, stage_num-1); % count false negative # in each bin (c, p)
S2_mm = zeros(size(S2)); % S(tau-(i), tau-(i-1))
S2_FN_mm = zeros(size(S2)); % S+(tau-(i), tau-(i-1))

for i=2:stage_num-1
    for x=1:length(labels)
        S2(binids(x, i), binids(x, i-1), i) = S2(binids(x, i), binids(x, i-1), i)+1;
        S2_FN(binids(x, i), binids(x, i-1), i) = S2_FN(binids(x, i), binids(x, i-1), i) + (labels(x)+1)/2;
    end
    % S2
    cum_y = cumsum(S2(:,:,i), 1);
    cum_yx = cumsum(cum_y, 2);
    S2_mm(2:end, 2:end, i) = cum_yx(1:end-1, 1:end-1);
    % S2_FN
    cum_y = cumsum(S2_FN(:,:,i), 1);
    cum_yx = cumsum(cum_y, 2);
    S2_FN_mm(2:end,2:end,i) = cum_yx(1:end-1, 1:end-1);
end

S = length(labels); % sample total number

% compute dp tables
T1 = zeros(bin_num, stage_num-1); % Ci * [S(tau-)+S(tau+)]/S
T3 = zeros(size(T1)); % S+(tau-)/S
T4 = cell(stage_num-1, 1); T4{1} = sparse(bin_num, bin_num);
T6 = cell(stage_num-1, 1); T6{1} = sparse(bin_num, bin_num);
C = cumsum(evalcost); C = C/C(end); % accumulate and normalize the computational cost in each stage
for i=1:stage_num-1
    T1(:,i) =  S_N(:,i)*(C(i)/S);
    T3(:,i) = S_FN(:,i)/S;
end

for i=2:stage_num-1
    T4{i} = S2_mm(:, :, i)*(C(i)/S);
    T6{i} = S2_FN_mm(:, :, i)/S;
end
% add the computational cost and FN term from the last stage
T1(:, end) = T1(:, end) + 1;
T3(:, end) = T3(:, end) + sum(scores(:,end)<classify_line & labels>0)/S;
% s2_mm = histc(scores(scores(:,end)<classify_line, stage_num-1), bin_edges(:,stage_num-1));
s2_mm = histc(scores(:, stage_num-1), bin_edges(:,stage_num-1));
s2_mm = s2_mm(1:end-1);
s2_mm = cumsum(s2_mm); s2_mm = [0; s2_mm(1:end-1)];% sample # reject by both the last two stages
s2_fn_mm = histc(scores(scores(:,end)<classify_line & labels>0, stage_num-1), bin_edges(:,stage_num-1));
s2_fn_mm = s2_fn_mm(1:end-1);
s2_fn_mm = cumsum(s2_fn_mm); s2_fn_mm = [0; s2_fn_mm(1:end-1)]; % false negative # reject by both the last two stages
T4{end} = T4{end} + repmat(s2_mm/S, [1, bin_num]);
T6{end} = T6{end} + repmat(s2_fn_mm/S, [1, bin_num]);

function [policy, upperbound, B, T] = DPsolver(CFN, T1, T3, T4, T6)
stage_num = size(T1, 2)+1;
D = size(T1, 1); % bin number
B = zeros(D, stage_num-1); % DP talbe
T = zeros(D, stage_num-1); % back trace table
B(:, 1) = T1(:,1) + T3(:,1)*CFN - T4{1}(:,1) - T6{1}(:,1)*CFN;
for i=2:stage_num-1
    B(:,i) = T1(:,i)+ T3(:,i)*CFN;
    R2 = T4{i} + T6{i}*CFN;
    for j=1:D
        [val, T(j,i-1)] = min(B(:, i-1) - R2(j, :)');
        B(j,i) = B(j,i)+val;
    end
end
policy = zeros(stage_num, 1);
policy(end) = 2;
[upperbound, policy(end-1)] = min(B(:, end));
for i=stage_num-2:-1:1
    policy(i) = T(policy(i+1), i);
end


function [risk, FPrate, FNrate, avgCost] = evalRisk(CFN, evalcost, policy, binids, labels)
policy = [policy(:,1), Inf(size(policy))]; policy(end, 2) = policy(end, 1);
[avgCost, FPrate, FNrate, FP, FN] = evalPolicy(policy, evalcost, binids, labels);
S = length(labels);
risk = CFN*FN/S + avgCost/sum(evalcost);


function policy = refineDP(policy, CFN, eval_cost, binids, labels, bin_num)
stage_num = size(binids,2);
is_updated = true;
[risk, FPrate, FNrate, avgCost] = evalRisk(CFN, eval_cost, policy, binids, labels);
while(is_updated)
    is_updated = false;
    for i=1:stage_num-1
        if policy(i,1)<bin_num
            c_policy = policy; c_policy(i,1) = c_policy(i,1)+1;
            if i==stage_num, c_policy(i,2) = c_policy(i,1); end
            [c_risk, c_FPrate, c_FNrate, c_avgCost] = evalRisk(CFN, eval_cost, c_policy, binids, labels);
            if c_risk<risk
                fprintf('*');
                policy = c_policy; is_updated = true; risk = c_risk;
            elseif policy(i,1)>1;
                c_policy = policy; c_policy(i,1) = c_policy(i,1)-1;
                if i==stage_num, c_policy(i,2) = c_policy(i,1); end
                [c_risk, c_FPrate, c_FNrate, c_avgCost] = evalRisk(CFN, eval_cost, c_policy, binids, labels);
                if c_risk<risk
                    fprintf('*');
                    policy = c_policy; is_updated = true; risk = c_risk;
                end
            end
            
        end
    end
    fprintf('#')
end
fprintf('\n')

function real_policy = constructRealPolicy(policy, bin_edges, classify_line)
stage_num = size(policy,1);
real_policy = Inf(stage_num, 2);
for i=1:stage_num-1
    real_policy(i,1) = bin_edges(policy(i,1), i);
end
real_policy(stage_num, :) = classify_line;


function [avgCost, FNrate] = evalUpperbound(T1, T3, T4, T6, policy, labels)
stage_num = length(T4)+1;
avgCost = T1(policy(1,1), 1) - T4{1}(policy(1,1), 1);
FN = T3(policy(1,1),1) - T6{1}(policy(1,1), 1);

for i=2:stage_num-1
    c = policy(i,1);
    p = policy(i-1,1);
    avgCost = avgCost + T1(c,i) - T4{i}(c,p);
    FN = FN + T3(c,i) - T6{i}(c,p);
end

S = length(labels);
FNrate = FN*S/sum(labels>0);
