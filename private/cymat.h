#pragma once

#include <cassert>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <algorithm>

/*
#define CYSINGLE 0
#define	CYDOUBLE 1
#define CYINT32 2
#define CYUINT8 3
*/
using namespace std;


template<typename T> class cyMat
{
public:
	int _h, _w, _c;
	int _hw;
	T *_data;
	bool _free;
	
	//methods
	cyMat(){ _c = 1;  _h = _w = _hw = 0; _data = NULL; _free = false; }

	cyMat(const cyMat<T> &src){
		_h = src._h; _w = src._w; _c = src._c; _hw = src._hw; 
		_free = src._free;
		_data = new T[_hw*_c];
		memcpy(_data, src._data, _hw*_c*sizeof(T));
	}

	cyMat(int h, int w){
		assert(h>0 && w > 0);
		_c = 1;
		_h = h; _w = w; _data = new T[h*w]; _free = true;
		_hw = h*w;
	}

	cyMat(int h, int w, T val){
		assert(h>0 && w > 0);
		_c = 1;
		_h = h; _w = w; _data = new T[h*w]; _free = true;
		for (int i = 0; i < h*w; i++) _data[i] = val;	
		_hw = h*w;
	}

	cyMat(int size[3])
	{
		assert(size[0]>0 && size[1] > 0 && size[2] > 0);
		_h = size[0]; _w = size[1]; _c = size[2];
		_hw = h*w;
		_data = new T[hw*_c]; _free = true;
	}

	cyMat(int size[3], T val)
	{
		assert(size[0]>0 && size[1] > 0 && size[2] > 0);
		_h = size[0]; _w = size[1]; _c = size[2];
		_hw = h*w;
		_data = new T[hw*_c]; _free = true;
		for (int i = 0; i<_hw*_c; i++) _data[i] = val;
	}

	void init(int h, int w){
		clear(); 
		assert(h>0 && w > 0);
		_c = 1;
		_h = h; _w = w; _data = new T[h*w]; _free = true;
		_hw = h*w;
	}

	void init(int h, int w, int val){
		clear();
		assert(h>0 && w > 0);
		_c = 1;
		_h = h; _w = w; _data = new T[h*w]; _free = true;
		for (int i = 0; i < h*w; i++) _data[i] = val;
		_hw = h*w;
	}

	void init(int size[3])
	{
		clear();
		assert(size[0]>0 && size[1] > 0 && size[2] > 0);
		_h = size[0]; _w = size[1]; _c = size[2];
		_hw = _h*_w;
		_data = new T[_hw*_c]; _free = true;
	}

	void init(T val)
	{
		for (int i = 0; i < _hw*_c; i++)
			_data[i] = val;

	}

	int size() const
	{
		return _hw;
	}

	T& val(int r, int c) { return _data[c*_h + r]; }
	T& val(int r, int c, int ch) { return _data[c*_h + r + ch*hw]; }
	T& val(int idx) { return _data[idx]; }


	const T& val(int r, int c) const { return _data[c*_h + r]; }
	const T& val(int r, int c, int ch) const { return _data[c*_h + r + ch*hw]; }
	const T& val(int idx) const { return _data[idx]; }


	cyMat<T>& operator = (const cyMat<T>& x){
		int size[3] = { x._h, x._w, x._c };
		this->init(size);
		memcpy(this->_data, x._data, x._hw*x._c*sizeof(T));
		return *this;
	}

	void clear() {
		if (_h > 0 && _w > 0)
		{
			delete _data;
			_h = 0; _w = 0; _c = 1; _hw = 0; _data = NULL; _free = false;
		}
	}
	
	~cyMat()
	{ 
		if (_free){
			delete[] _data;
			_h = 0; _w = 0; _c = 1; _hw = 0; _data = NULL; _free = false;
		}
	}

	
	void printInfo()
	{
		mexPrintf("h: %d w: %d c: %d\n", _h, _w, _c);
	}
	
	void print()
	{
		for (int r = 0; r < _h; r++)
		{
			for (int c = 0; c < _w; c++)
				cout << val(r, c) << " ";
			cout << endl;
		}
	}
};


template<typename T>
cyMat<T> cumsum(const cyMat<T> &A, int dim){
	cyMat<T> rt(A._h, A._w);
	switch (dim){
	case 1: 
		for (int c = 0; c < A._w; c++) {
			rt.val(0, c) = A.val(0, c);
			for (int r = 1; r < A._h; r++) rt.val(r, c) = rt.val(r - 1, c) + A.val(r, c);
		}
		break;
	case 2:
		for (int r = 0; r < A._h; r++) {
			rt.val(r, 0) = A.val(r, 0);
			for (int c = 1; c < A._w; c++) rt.val(r, c) = rt.val(r, c - 1) + A.val(r, c);
		}
		break;
	}
	return rt;
}

template<typename T>
cyMat<T> invcumsum(const cyMat<T> &A, int dim){
	cyMat<T> rt(A._h, A._w);
	switch (dim){
	case 1:
		for (int c = 0; c < A._w; c++) {
			rt.val(A._h-1, c) = A.val(A._h-1, c);
			for (int r = A._h-2; r > -1; r--) rt.val(r, c) = rt.val(r + 1, c) + A.val(r, c);
		}
		break;
	case 2:
		for (int r = 0; r < A._h; r++) {
			rt.val(r, A._w-1) = A.val(r, A._w-1);
			for (int c = A._w-2; c > -1; c--) rt.val(r, c) = rt.val(r, c + 1) + A.val(r, c);
		}
		break;
	}
	return rt;
}

template<typename T>
cyMat<T> max(const cyMat<T> &A)
{
	cyMat<T> rt(1, A._w);
	for (int c = 0; c < A._w; c++)
		rt.val(c) = A.val(0, c);
	for (int r = 1; r < A._h; r++)
		for (int c = 0; c < A._w; c++)
			rt.val(c) = max(rt.val(c), A.val(r, c));
	return rt;
}

template<typename T>
cyMat<T> min(const cyMat<T> &A)
{
	cyMat<T> rt(1, A._w);
	for (int c = 0; c < A._w; c++)
		rt.val(c) = A.val(0, c);
	for (int r = 1; r < A._h; r++)
		for (int c = 0; c < A._w; c++)
			rt.val(c) = min(rt.val(c), A.val(r, c));
	return rt;
}

template <typename T>
void sort_index(cyMat<int> & out, const cyMat<T> &A)
{
	out.init(A._h, A._w);
	vector<int> idx(A._h);
	for (int c = 0; c < A._w; c++)
	{
		for (int i = 0; i < A._h; i++)
			idx[i] = i;
		sort(idx.begin(), idx.end(), [&A, c](int i1, int i2) {return A.val(i1, c) < A.val(i2, c); });
		for (int i = 0; i < A._h; i++)
			out.val(i, c) = idx[i];
	}
}

cyMat<int> randperm(int K);


typedef cyMat<int> cyMati;
typedef cyMat<float> cyMatf;
typedef cyMat<double> cyMatd;