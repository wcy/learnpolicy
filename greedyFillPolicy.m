function policy = greedyFillPolicy(Spos, Sneg, init_policy)
% return [minimum reject thresholds, maximum accept thresholds] which guarantee:
% a positive sample is not rejected by the reject thresholds if it's not rejected by the initial reject thresholds;
% a negative sample is not accepted by the accept thresholds if it's not accepted by the initial accept thresholds;
%
% USAGE
%	policy = greedyFillPolicy(Spos, Sneg, init_policy)

policy = mexGreedyFillPolicy(double(Spos), double(Sneg), init_policy);