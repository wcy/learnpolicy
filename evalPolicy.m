function [avgCost, FPrate, FNrate, FP, FN, avgCost_pos, avgCost_neg] = evalPolicy(policy, score_pos, score_neg, cost)
% evaluate decision policy
%
% USAGE
%	[avgCost, FPrate, FNrate, FP, FN, avgCost_pos, avgCost_neg] = evalPolicy(policy, cost, score, labels)
%
% INPUTS
%	policy - [m*2] decision policy ([rej_thresh, acp_thresh], rej_thresh(end) = acp_thresh(end) is required)
%	score_pos [n*m] score matrix for n positive samples
%	score_neg - score matrix for negative samples
% 	cost - [m*1] computation cost for each weak learner or stage.
% OUTPUTS
%	avgCost - average computation cost
%   FPrate - false positive rate
%	FNrate - false negative rate
%	FP - false positive sample number
%	FN - false negative sample number

%% Mex Implementation
[N, M] = size(score_pos); 
assert(size(policy, 1) == M && size(policy,2) == 2)
assert(length(cost) == M);

% [FPrate, FNrate, avgCost, FP, FN, avgCost_pos, avgCost_neg] = mexEvalPolicy(double(policy), double(score), double(labels), double(cost));
[FPrate, FNrate, avgCost, FP, FN, avgCost_pos, avgCost_neg] = mexEvalPolicy(double(policy), double(score_pos), double(score_neg), double(cost));
%% Simple Matlab Implementation
% b_alive = true(length(labels), 1);
% M = size(score,2);
% FP = 0;
% FN = 0;
% avgCost = 0;
% b_pos = labels>0;
% b_neg = ~b_pos;
% for i=1:M
%     avgCost = avgCost + sum(b_alive)*cost(i);
%     y = score(:, i);
%     FP = FP + sum(y>=policy(i,2) & b_alive & b_neg);
%     FN = FN + sum(y<policy(i,1) & b_alive & b_pos);
%     b_alive = b_alive & y<policy(i,2) & y>=policy(i,1);
% end
% 
% avgCost = avgCost / length(labels);
% FPrate = FP/sum(labels<=0);
% FNrate = FN/sum(labels>0);