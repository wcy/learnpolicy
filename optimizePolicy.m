function [policy, trainInfo] = optimizePolicy(score_pos, score_neg, cost_per_weak_learner, final_classif_thresh, opts)
% Given target FP, FN rate, optimize decision policy (early reject/accept
% thresholds) for an ensemble of additive weak learners/classiers for
% binary classification
% a sample is early accepted if its score >= accept_threshold
% a sample is early rejected if its score < reject_threshold
% USAGE
% [policy, trainInfo] = optimizePolicy(score_pos, score_neg, cost_per_weak_learner, final_classif_thresh, opts)
%
% This function optimize the following objective:
%       minimize evaluation_cost(reject thresholds, accept thresholds)
%       s.t. (|FP - target_FP| < opts.fp_epsilon)
%           & (|FN - target_FN| < opts.fn_epsilon), if opts.mode=='full'
%       or s.t. |FN - target_FN| < opts.fn_epsilon, if opts.mode=='rej'
%
% Optimization algorithm:
%   step 1: divide all the weak learners into a few stages (20 by default),
%   for simplicity. The last stage's threshold is a fixed parameter
%   (final_classif_thresh). The thresholds of remaining stages are optimized.
%       The score of the last weak learner in each stage is used as the stage's score.
%   step 2: iteratively optimize the objective. The objective is splitted into two parts:
%          minimize evaluation_cost(reject thresholds) of negative samples, s.t. |FN - target_FN| < fn_epsilon
%          minimize evaluation_cost(accept thresholds) of positive samples, s.t. |FP - target_FP| < fp_epsilon
%       We iteratively optimize one of the two parts via coordinate descent and stop until convergence
%   step 3: given the optimized threhsolds for all the stages, we set the
%   thresholds of remaining weak learners in a conservative and safe
%   manner. That is, these thresholds do not accept negative samples or
%   reject positive samples if they are correctly accepted or rejected by the thresholds output by step 2.
%   In other words,
%         reject threshold = min_value(positive samples accepted by the current thresholds)
%         accept threshold = max_value(negative samples rejected by the current thresholds) + a small value
%
% INPUTS
%   let there be m weak learners
%   score_pos - [n_pos*m] score matrix for positive samples
%   score_neg - [n_neg*m] score matrix for negative samples
%   cost_per_weak_learner - [m*1] computation cost per weak learner
%   final_classif_thresh - classification threshold of the full classifier
%   opts   - parameters
%      .stage_end_idx - [t*1] split m weak learners into t stages, each element
%                             marks the idx of the end of each
%                             stage. Coordinate descent optimization is applied
%                             on the thresholds of the t stages.
%                             Recommend to use 20-40 stages. Too
%                             many stages may be uncessary for better
%                             result but make the optimization process
%                             slow.
%      .mode - 'rej' only optimize reject thresholds;
%              'full' optimize both accept & reject thresholds;
%      .tgfp - target false positive rate [not required if mode='rej']
%      .tgfn - target false negative rate
%      .fp_epsilon - [not required if mode = 'rej']
%      .fn_epsilon - iteration stops if current optimized policy's fp & fn
%                    satisfy abs(fn-tgfn)<fn_epsilon (&&
%                    abs(fp-tgfp)<fp_epsilon [not required if mod='rej'])
%      .lambda_epsilon - iteration stops if the difference between the
%                 upper and lower bound of the hyperparameter
%                 lambda_fn/lambda_fp is smaller than .lambda_epsilon. (0.001 by default)
%      .bin_num - bin number for discretizing score values. can be set very
%                 large (10000 by default), has minor impact on speed.
%
% OUTPUTS
%   policy - [m*2] [rejection thresholds, accept thresholds]
%   trainInfo - intermediate results of training

[pos_num, weak_learner_num] = size(score_pos);
assert(weak_learner_num == size(score_neg, 2));
assert(weak_learner_num == length(cost_per_weak_learner));

% Check if opts is valid
assert(isfield(opts, 'mode'));
assert(strcmpi(opts.mode, 'rej') || strcmpi(opts.mode, 'full'));
assert(isfield(opts, 'bin_num'));
assert(isfield(opts, 'stage_end_idx'));
assert(opts.stage_end_idx(end) == weak_learner_num);
assert(isfield(opts, 'tgfn') && isfield(opts, 'fn_epsilon'));
if strcmpi(opts.mode, 'full')
    assert(isfield(opts, 'tgfp') && isfield(opts, 'fp_epsilon'));
end

stage_num = length(opts.stage_end_idx);
% compute computational cost in terms of stages.
cost_cumsum_per_weak_learner = cumsum(cost_per_weak_learner(:));
cost_cumsum_per_stage = cost_cumsum_per_weak_learner(opts.stage_end_idx);
cost_per_stage = [cost_cumsum_per_stage(1); diff(cost_cumsum_per_stage)];

% discretize scores to bins
% note that the last stage only has 2 bins (1 for reject, 2 for accept), determined by
% final_classif_thresh
[bin_edges, bin_ids] = constructBin([score_pos(:, opts.stage_end_idx); score_neg(:, opts.stage_end_idx)], ...
    opts.bin_num, final_classif_thresh);
bin_ids_pos = bin_ids(1:pos_num, :);
bin_ids_neg = bin_ids(pos_num+1:end, :);
% assign early accept/reject thresholds with initial values.
acp_threshs = [ones(stage_num-1, 1)*opts.bin_num; 2]; % the maximum value in terms of bin index
rej_threshs = [ones(stage_num-1, 1); 2]; % the minimum value in terms of bin index

trainInfo = [];
trainInfo.mid_opt_rt = [];

% optimize with coordinate descent
if strcmpi(opts.mode, 'rej') % only optimize rejection thresholds (fast)
    [rej_threshs, trainInfo] = optmizeRejLines(bin_ids_pos, bin_ids_neg, rej_threshs, acp_threshs, cost_per_stage, opts, trainInfo);
else % optimize both rejection and accept thresholds, maybe slow because of searching for Lambda
    while true
        prev_acp_threshs = acp_threshs;
        prev_rej_threshs = rej_threshs;
        [rej_threshs, trainInfo] = optmizeRejLines(bin_ids_pos, bin_ids_neg, rej_threshs, acp_threshs, cost_per_stage, opts, trainInfo);
        [cost, fp, fn, ~] = evalPolicy([rej_threshs, acp_threshs], bin_ids_pos, bin_ids_neg, cost_per_stage);
        if sum(prev_rej_threshs ~= rej_threshs)==0, fprintf('Converged!\n'); break; end
        if abs(fn - opts.tgfn) <= opts.fn_epsilon && abs(fp - opts.tgfp) <= opts.fp_epsilon
            fprintf('Converged!\n');
            break;
        end
        [acp_threshs, trainInfo] = optmizeAcpLines(bin_ids_pos, bin_ids_neg, rej_threshs, acp_threshs, cost_per_stage, opts, trainInfo);
        if sum(prev_acp_threshs ~= acp_threshs)==0, fprintf('Converged!\n'); break; end
        [cost, fp, fn, ~] = evalPolicy([rej_threshs, acp_threshs], bin_ids_pos, bin_ids_neg, cost_per_stage);
        if abs(fn - opts.tgfn) <= opts.fn_epsilon && abs(fp - opts.tgfp) <= opts.fp_epsilon
            fprintf('Converged!\n');
            break;
        end
    end
end

policy = [-inf(weak_learner_num, 1), inf(weak_learner_num, 1)];
% fill the optimized thresholds
policy(opts.stage_end_idx(1:end-1), 1) = bin_edges(rej_threshs(1:end-1) + (0:stage_num-2)'*opts.bin_num);
policy(opts.stage_end_idx(1:end-1), 2) = bin_edges(acp_threshs(1:end-1) + (0:stage_num-2)'*opts.bin_num);
policy(end, :) = final_classif_thresh;
fprintf('Greedy Fill all thresholds\n');
policy = greedyFillPolicy(score_pos, score_neg, policy);% fill the remaining thresholds

if strcmpi(opts.mode, 'rej'), policy(1:end-1, 2) = inf; end
[cost, fp, fn, ~, ~, cost_pos, cost_neg] = evalPolicy(policy, score_pos, score_neg, cost_per_weak_learner);
if strcmpi(opts.mode, 'full')
    fprintf('Result policy: cost %f, FP: %f, FN: %f (target accuray: tgfp: %f, tgfn: %f )\n', cost, fp, fn, opts.tgfp, opts.tgfn);
    fprintf('Positive samples on average takes %f\n', cost_pos);
else
    fprintf('Result policy: cost %f, FP: %f, FN: %f (target accuray: tgfn: %f )\n', cost, fp, fn, opts.tgfn);
end
fprintf('Negative samples on average takes %f\n', cost_neg);

% coordinate descent with random iteration order, results maybe different
% in different runs, but the overall optimization cost shall be similar.
function rej_threshs = optmizeRejLinesFnc(bin_ids_pos, bin_ids_neg, init_rej_threshs, acp_threshs, cost_cumsum, bin_num, lambda)
cost_cumsum = cost_cumsum/cost_cumsum(end); % normalize cost
rej_threshs = mexOptimizeRejline(int32(init_rej_threshs-1), int32(acp_threshs-1), ...
    int32(bin_ids_pos-1), int32(bin_ids_neg-1), double(cost_cumsum), bin_num, lambda );
rej_threshs = double(rej_threshs)+1;

function acp_threshs = optmizeAcpLinesFnc(bin_ids_pos, bin_ids_neg, rej_threshs, init_acp_threshs, cost_cumsum, bin_num, lambda)
cost_cumsum = cost_cumsum/cost_cumsum(end); % normalize cost
acp_threshs = mexOptimizeAcpline(int32(rej_threshs-1), int32(init_acp_threshs-1), ...
    int32(bin_ids_pos-1), int32(bin_ids_neg-1), double(cost_cumsum), bin_num, lambda );
acp_threshs = 1+double(acp_threshs);

function trainInfo = updateInfo(trainInfo, lambda_fp, lambda_fn, fp, fn, cost, policy)
info.lambda_fn = lambda_fp;
info.lambda_fp = lambda_fn;
info.cost = cost;
info.policy = policy;
info.fn = fn;
info.fp = fp;
trainInfo.mid_opt_rt = [trainInfo.mid_opt_rt; info];

% this function cast the original optimization objective:
%   min evaluate_cost_of_negative_samples, s.t. |FN-target_FN|<fn_epsilon
% to: min evaluate_cost_of_negative_samples + lambda_fn*FN,
% where lambda_fn is a hyperparameter which is searched via binary search,
% s.t. |FN-target_FN|<fn_epsilon
% at initial iterations, lambda_fn tends to be too large, so it decreases.
% as a result, FN gradually increases. Then it bumps around the target value till converge.
function [rej_threshs, trainInfo] = optmizeRejLines(bin_ids_pos, bin_ids_neg, rej_threshs, acp_threshs, cost_per_stage, opts, trainInfo)
% step 1, for each stage, find the maximum reject threshold that does not reject a positive sample which arrives here, which is the minimum of such positive sample scores
rej_threshs = double(mexRefineRejThreshs_int32(int32(bin_ids_pos), int32(rej_threshs), int32(acp_threshs)));
% step 2, such thresholds are conserative and serve as lower bound, evaluate their
% cost on training data, which is upper bound of cost
[~, fp0, fn0, ~, ~, ~, upbnd_cost] = evalPolicy([rej_threshs, acp_threshs], bin_ids_pos, bin_ids_neg, cost_per_stage);
fprintf('uper bnd cost: %f, fp0: %f, fn0: %f\n', upbnd_cost, fp0, fn0);
fn_epsilon = opts.fn_epsilon;
cost_cumsum = cumsum(cost_per_stage);
% step 3, estimate the upperbound of lambda_fn by the following
% proposition:
%  upbnd_cost + fn0*lambda_fn > lambda_fn*target_FN
max_lambda_fn = upbnd_cost / (max(opts.tgfn - fn0, fn_epsilon) * cost_cumsum(end));% use fn_epsilon to avoid Inf value
min_lambda_fn = 0;

% step 4, binary search for lambda_fn to reach target fn.
% at initial iterations, FN gradually increases. Then it bumps around the target value.
while fn0 <= opts.tgfn
    lambda_fn = (max_lambda_fn + min_lambda_fn)/2;
    % minimize evaluate_cost_of_negative_samples + lambda_fn*FN via
    % coordinate descent
    tmp_rej_threshs = optmizeRejLinesFnc(bin_ids_pos, bin_ids_neg, rej_threshs, acp_threshs, cost_cumsum, opts.bin_num, lambda_fn);
    [cost, fp, fn, ~, ~, ~, cost_neg] = evalPolicy([tmp_rej_threshs, acp_threshs], bin_ids_pos, bin_ids_neg, cost_per_stage);
    trainInfo = updateInfo(trainInfo, -1, lambda_fn, fp, fn, cost, [tmp_rej_threshs, acp_threshs]);
    fprintf('Itr: %d, lambdaFN: %f, cost: %f, negative sample cost: %f, fp: %f, fn: %f\n', length(trainInfo.mid_opt_rt), lambda_fn, cost, cost_neg, fp, fn);
    if abs(fn - opts.tgfn) <= fn_epsilon
        rej_threshs = tmp_rej_threshs;
        fprintf('Succeed, reach desired FN\n');
        break;
    end
    if fn > opts.tgfn
        min_lambda_fn = lambda_fn;
    end
    if fn < opts.tgfn
        if fn > fn0
            rej_threshs = tmp_rej_threshs; % it is beneficial to use currently optimized thresholds as initual value, which help cd optimization convergences faster.
        end
        max_lambda_fn = lambda_fn;
    end
    if (max_lambda_fn - min_lambda_fn) <=  opts.lambda_epsilon
        rej_threshs = tmp_rej_threshs;
        fprintf('Converged (fail to reach desired FN)\n');
        break;
    end
end
assert(sum(rej_threshs>acp_threshs) == 0); % check if reject thresholds is no larger than accept thresholds, shall never happens.

function [acp_threshs, trainInfo] = optmizeAcpLines(bin_ids_pos, bin_ids_neg, rej_threshs, acp_threshs, cost_per_stage, opts, trainInfo)
% step 1, minimize accept thresholds such that no negative sample is
% accepted if it's originally rejected by the input initial thresholds
acp_threshs = double(mexRefineAcpThreshs_int32(int32(bin_ids_neg), int32(rej_threshs), int32(acp_threshs)));
% step 2, such thresholds are conserative and serve as lower bound, evaluate their
% cost on training data, which is upper bound of cost.
[~, fp0, fn0, ~, ~, upbnd_cost, ~] = evalPolicy([rej_threshs, acp_threshs], bin_ids_pos, bin_ids_neg, cost_per_stage);
fprintf('uper bnd cost: %f, fp0: %f, fn0: %f\n', upbnd_cost, fp0, fn0);
fp_epsilon = opts.fp_epsilon;
cost_cumsum = cumsum(cost_per_stage);
% step 3, estimate the upperbound of lambda_fp by the following
% proposition:
%  upbnd_cost + fn0*lambda_fp > lambda_fp*target_FP
max_lambda_fp = upbnd_cost / (max(opts.tgfp - fp0, fp_epsilon)*cost_cumsum(end));
min_lambda_fp = 0;
% binary search for lambda_fp to reach target fp.
% at initial iterations, FP gradually increase. Then it bumps around the target value.
while fp0 <= opts.tgfp
    lambda_fp = (max_lambda_fp + min_lambda_fp)/2;
    % minimize evaluate_cost_of_positive_samples + lambda_fp*FP via
    % coordinate descent
    tmp_acp_threshs = optmizeAcpLinesFnc(bin_ids_pos, bin_ids_neg, rej_threshs, acp_threshs, cost_cumsum, opts.bin_num, lambda_fp);
    [cost, fp, fn, ~, ~, cost_pos, ~] = evalPolicy([rej_threshs, tmp_acp_threshs], bin_ids_pos, bin_ids_neg, cost_per_stage);
    trainInfo = updateInfo(trainInfo, lambda_fp, -1, fp, fn, cost, [rej_threshs, tmp_acp_threshs]);
    fprintf('Itr: %d, lambdaFP: %f, cost: %f, positive sample cost: %f, fp: %f, fn: %f\n', length(trainInfo.mid_opt_rt), lambda_fp, cost, cost_pos, fp, fn);
    if abs(fp - opts.tgfp) <= fp_epsilon
        acp_threshs = tmp_acp_threshs;
        fprintf('Succeed, reach desired FP\n');
        break;
    end
    if fp > opts.tgfp
        min_lambda_fp = lambda_fp;
    end
    if fp < opts.tgfp
        if fp > fp0
            acp_threshs = tmp_acp_threshs;  % it is beneficial to use currently optimized thresholds as initual value, which help cd optimization convergences faster.
        end
        max_lambda_fp = lambda_fp;
    end
    if (max_lambda_fp - min_lambda_fp) <=  opts.lambda_epsilon
        acp_threshs = tmp_acp_threshs;
        fprintf('Converged (fail to reach desired FP)\n');
        break;
    end
end
assert(sum(rej_threshs>acp_threshs) == 0); % check if reject thresholds is no larger than accept thresholds, shall never happens.