#include "LearnPolicy.h"
#include "cymatmex.h"

void mexFunction(int nl, mxArray *pl[], int nr, const mxArray *pr[])
{
	cyMat<int> Spos;
	cyMat<int> init_rej_threshs, rej_threshs, acp_threshs;
	mxMat2cyMat(pr[0], Spos);
	mxMat2cyMat(pr[1], init_rej_threshs);
	mxMat2cyMat(pr[2], acp_threshs);
	rej_threshs = init_rej_threshs;
	refineRejline(rej_threshs, acp_threshs, Spos);
	pl[0] = cyMat2mxMat(rej_threshs);
}