#include "LearnPolicy.h"
#include "cymatmex.h"

void mexFunction(int nl, mxArray *pl[], int nr, const mxArray *pr[])
{
	cyMat<double> Spos, Sneg;
	cyMat<double> init_policy;
	mxMat2cyMat(pr[0], Spos);
	mxMat2cyMat(pr[1], Sneg);
	mxMat2cyMat(pr[2], init_policy);

	cyMat<double> policy = greedyRefinePolicy(Spos, Sneg, init_policy);
	pl[0] = cyMat2mxMat(policy);
}
