#include "LearnPolicy.h"
#include "cymatmex.h"
#include "cymat.h"

void mexFunction(int nl, mxArray *pl[], int nr, const mxArray *pr[])
{
	cyMat<int> Spos, Sneg, rej_lines, init_accept_lines;
	cyMat<double> C;
	int binnum;
	double lambda;
	mxMat2cyMat(pr[0], rej_lines);
	mxMat2cyMat(pr[1], init_accept_lines);
	mxMat2cyMat(pr[2], Spos);
	mxMat2cyMat(pr[3], Sneg);
	mxMat2cyMat(pr[4], C);
	binnum = mxGetScalar(pr[5]);
	lambda = mxGetScalar(pr[6]);

	//cyMat<int> rej_lines = init_rej_lines;

	cyMat<int>acp_lines = optmizeAcpLine(rej_lines, init_accept_lines, Spos, Sneg, C, lambda, binnum);
	pl[0] = cyMat2mxMat(acp_lines);

}