%% compile mex codes here
% cd private
% Compile
% cd ..
%% load test data
% load('test_data.mat', 'score_pos', 'score_neg', 'c')
%% only optimize early rejection thresholds
final_classif_thresh = 0.839249; % the threshold used in the final svm classifier
fn0 = sum(score_pos(:,end) < final_classif_thresh) / size(score_pos, 1);
opts = getDefaultOpts(1081, fn0+0.01)
% for our case, 1080 weak learners + 1 final svm, we use 21 stages, 20 for 1080, 1 for svm
opts.stage_end_idx = [54:54:1080, 1081];

[policy, Info] = optimizePolicy(score_pos, score_neg, c, final_classif_thresh, opts);

%% optimize both early rejection and early accept thresholds
final_classif_thresh = 0.839249;
fn0 = sum(score_pos(:,end) < final_classif_thresh) / size(score_pos, 1);
fp0 = sum(score_neg(:,end) >= final_classif_thresh) / size(score_neg, 1);
opts = getDefaultOpts(1081, fn0+0.01, fp0)
% for our case, 1080 weak learners + 1 final svm, we use 21 stages, 20 for 1080, 1 for svm
opts.stage_end_idx = [54:54:1080, 1081];

[policy, Info] = optimizePolicy(score_pos, score_neg, c, final_classif_thresh, opts);
