#pragma once

#include "cymat.h"
#include "mex.h"
#include <typeinfo>
#include <vector>
#include <algorithm>



template<typename T>
void mxMat2cyMat(const mxArray* src, cyMat<T> &dst)
{
	dst.clear();
	mxClassID mxtype = mxGetClassID(src);

	switch (mxtype)
	{
	case mxSINGLE_CLASS:
		//mexPrintf("float\n");
		if (typeid(T) != typeid(float))
			return;
		break;
	case mxDOUBLE_CLASS:
		//mexPrintf("double %s\n");
		if (typeid(T) != typeid(double))
			return;
		break;
	case mxINT32_CLASS:
		//mexPrintf("int\n");
		if (typeid(T) != typeid(int))
			return;
		break;
	case mxUINT8_CLASS:
		//mexPrintf("unint8\n");
		if (typeid(T) != typeid(unsigned char))
			return;
		break;
	default:
		return;
	}
	const int* size = mxGetDimensions(src);
	int dims = mxGetNumberOfDimensions(src);
	assert(dims == 2 || dims == 3);
	dst._h = size[0]; dst._w = size[1];
	if (dims == 3)
		dst._c = size[2];
	else
		dst._c = 1;
	dst._hw = dst._h*dst._w;
	dst._free = false;
	dst._data = (T*)mxGetData(src);
	//mexPrintf("first val: %f\n", dst._data[0]);

}

template<typename T>
mxArray* cyMat2mxMat(cyMat<T> &src)
{
	mxArray* rt = NULL;
	mxClassID type;
	if (typeid(T) == typeid(double))
		type = mxDOUBLE_CLASS;
	else if (typeid(T) == typeid(float))
		type = mxSINGLE_CLASS;
	else if (typeid(T) == typeid(int))
		type = mxINT32_CLASS;
	else if (typeid(T) == typeid(unsigned char))
		type = mxUINT8_CLASS;
	else
	{
		mexPrintf("unsupported type\n");
		return rt;
	}

	//mexPrintf("create mxarray\n");
	if (src._c == 1)
		rt = mxCreateNumericMatrix(src._h, src._w, type, mxREAL);
	else
	{
		mwSize size[3];
		size[0] = src._h; size[1] = src._w; size[2] = src._c;
		rt = mxCreateNumericArray(3, size, type, mxREAL);
	}

	T* data = (T*)mxGetData(rt);
	//mexPrintf("copy %d * %d * %d from %f\n", src._hw, src._c, sizeof(T), src._data[0]);
	memcpy(data, src._data, src._hw*src._c*sizeof(T));
	return rt;
}

template<typename T>
void mexPrintCyMat(cyMat<T> &mat)
{
	for (int r = 0; r < mat._h; r++)
	{
		for (int c = 0; c < mat._w; c++)
		{
			if (typeid(T) == typeid(float) || typeid(T) == typeid(double))
				mexPrintf("%f ", mat.val(r, c));
			else
				mexPrintf("%d ", (int)mat.val(r, c));
		}
		mexPrintf("\n");
	}
}






