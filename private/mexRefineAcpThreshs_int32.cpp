#include "LearnPolicy.h"
#include "cymatmex.h"

void mexFunction(int nl, mxArray *pl[], int nr, const mxArray *pr[])
{
	cyMat<int> Sneg;
	cyMat<int> init_acp_threshs, rej_threshs, acp_threshs;
	mxMat2cyMat(pr[0], Sneg);
	mxMat2cyMat(pr[1], rej_threshs);
	mxMat2cyMat(pr[2], init_acp_threshs);
	acp_threshs = init_acp_threshs;
	refineAcpline(rej_threshs, acp_threshs, Sneg);
	pl[0] = cyMat2mxMat(acp_threshs);
}