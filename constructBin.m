function [bin_edges, binids] = constructBin(score, bin_num, final_classif_thresh)
% discretize scores into bins
%
% USAGE
%	[bin_edges, binids] = constructBin(score, bin_num, final_classif_thresh)
%
% for each sample with m scores, first m-1 scores are discretized into 'bin_num' bins;
% the last score is discretized into 2 bins by the 'final_classif_thresh'

bin_edges = zeros(bin_num, size(score, 2));
bin_edges(1,:) = -inf;
bin_edges(end,:) = inf;
for i=1:size(score, 2)
    step_length = (max(score(:,i)) - min(score(:,i)))/(bin_num-3);
    bin_edges(2:end-1,i) = min(score(:,i)):step_length:min(score(:,i))+step_length*(bin_num-3);
end

[N, stage_num] = size(score);
binids = zeros(N, size(score, 2));
for i = 1 : stage_num-1
    [bincounts, binids(:,i)] = histc(score(:,i), bin_edges(:,i));
end
binids(score(:,end)<final_classif_thresh, end) = 1;
binids(score(:,end)>=final_classif_thresh, end) = 2;