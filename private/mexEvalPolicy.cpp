#include "cymat.h"
#include "cymatmex.h"
/*
void mexFunction(int nl, mxArray *pl[], int nr, const mxArray *pr[])
{
	cyMat<double> policy; //policy
	cyMat<double> D; //decision values
	cyMat<double> labels;
	cyMat<double> evalCost;

	mxMat2cyMat(pr[0], policy);
	mxMat2cyMat(pr[1], D);
	mxMat2cyMat(pr[2], labels);
	mxMat2cyMat(pr[3], evalCost);
	//mexPrintCyMat(policy);
	

	int N = labels._h;
	//mexPrintf("N = %d\n", N);
	int Npos = 0;
	int Nneg = 0;
	int FP = 0;
	int FN = 0;
	int stagenum = D._w;
	//mexPrintf("stage# = %d\n", stagenum);
	double cost, cost_pos, cost_neg = 0;

	for (int i = 0; i < N; i++)
	{
		if (labels.val(i) > 0)
			Npos++;
		else
			Nneg++;

		for (int s = 0; s < stagenum; s++)
		{
			cost += evalCost.val(s);
			if (labels.val(i) > 0)
				cost_pos += evalCost.val(s);
			else
				cost_neg += evalCost.val(s);
			if (D.val(i, s) < policy.val(s, 0))
			{
				if (labels.val(i) > 0)
					FN++;
				break;
			}
			else if (D.val(i, s) >= policy.val(s, 1))
			{
				if (labels.val(i) <= 0)
					FP++;
				break;
			}
			
		}
	}

	double avgCost = cost / N;
	double FPrate = (double)FP / (double)Nneg;
	double FNrate = (double)FN / (double)Npos;
	//mexPrintf("Npos %d, Nneg %d, FN %d, FP %d\n", Npos, Nneg, FN, FP);
	pl[0] = mxCreateDoubleScalar(FPrate);
	pl[1] = mxCreateDoubleScalar(FNrate);
	pl[2] = mxCreateDoubleScalar(avgCost);
	pl[3] = mxCreateDoubleScalar(FP);
	pl[4] = mxCreateDoubleScalar(FN);
	pl[5] = mxCreateDoubleScalar(cost_pos / Npos);
	pl[6] = mxCreateDoubleScalar(cost_neg / Nneg);
}
*/

struct EvalResult
{
	int FP, FN;
	double cost_pos, cost_neg;
	double cost;
};

EvalResult evalPolicy(const cyMat<double> &policy, const cyMat<double> &score_pos, const cyMat<double> &score_neg, const cyMat<double> &cost_per_stage)
{
	EvalResult eval_result;
	eval_result.cost_neg = 0;
	eval_result.cost_pos = 0;
	eval_result.FN = 0;
	eval_result.FP = 0;
	int Npos = score_pos._h;
	int Nneg = score_neg._h;
	int stagenum = score_pos._w;
	for (int i = 0; i < Npos; i++)
	{
		for (int t = 0; t < stagenum; t++)
		{
			eval_result.cost_pos += cost_per_stage.val(t);
			if (score_pos.val(i, t) >= policy.val(t, 1))
				break;
			if (score_pos.val(i, t) < policy.val(t, 0))
			{
				eval_result.FN++;
				break;
			}
		}
		
	}

	for (int i = 0; i < Nneg; i++)
	{
		for (int t = 0; t < stagenum; t++)
		{
			eval_result.cost_neg += cost_per_stage.val(t);
			if (score_neg.val(i, t) >= policy.val(t, 1))
			{
				eval_result.FP++;
				break;
			}
			if (score_neg.val(i, t) < policy.val(t, 0))
				break;
		}

	}

	eval_result.cost = (eval_result.cost_neg + eval_result.cost_pos) / (Npos + Nneg);
	eval_result.cost_pos = eval_result.cost_pos / Npos;
	eval_result.cost_neg = eval_result.cost_neg / Nneg;
	return eval_result;
}

void mexFunction(int nl, mxArray *pl[], int nr, const mxArray *pr[])
{
	cyMat<double> policy; //policy
	cyMat<double> score_pos, score_neg; //decision values
	cyMat<double> cost_per_stage;

	mxMat2cyMat(pr[0], policy);
	mxMat2cyMat(pr[1], score_pos);
	mxMat2cyMat(pr[2], score_neg);
	mxMat2cyMat(pr[3], cost_per_stage);

	EvalResult eval_result = evalPolicy(policy, score_pos, score_neg, cost_per_stage);

	pl[0] = mxCreateDoubleScalar((double)eval_result.FP/score_neg._h);
	pl[1] = mxCreateDoubleScalar((double)eval_result.FN/score_pos._h);
	pl[2] = mxCreateDoubleScalar(eval_result.cost);
	pl[3] = mxCreateDoubleScalar(eval_result.FP);
	pl[4] = mxCreateDoubleScalar(eval_result.FN);
	pl[5] = mxCreateDoubleScalar(eval_result.cost_pos);
	pl[6] = mxCreateDoubleScalar(eval_result.cost_neg);
}